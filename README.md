# firefly3_nginx_https_docker

Docker-Compose Firefly III Setup with Let's Encrypt HTTPS.

## What is this?

I wanted a (seemingly) simple thing: **ONE hermetic `docker-compose.yml’ for near-zero Firefly III setup**.

This turned out to be trickier than I thought, because there are effectively 4 moving parts:

1. `firefly_iii_app` aka the Firefly III web server listening on port 80
2. `firefly_iii_db` aka the Firefly III DB (usually Postgres)
3. `nginx` to reverse-proxy the Firefly III web server
4. Some kind of Let’s Encrypt automation that would work with (3)

Thankfully, there’s two out-of-the-box `nginx`-based Docker containers that can solve (3) and (4).

Unfortunately, the configuration instructions are one or more of:

+ Written poorly/using outdated config options
+ Written for plain Docker and not `docker-compose.yml`
+ Don’t cover setting up the networking properly

I got super frustrated and, with a lot of trial and error, came up with this config.

### Advantages

+ **Dead-simple**. Only requires basic Docker knowledge.
+ **Clean.** Only 1 file and a few commandline invocations.
+ **Hands-off**. Docker Compose will make sure that you get upgrades, and that the containers are restarted on host reboot.

### Disadvantages

+ I have no idea what will happen if/when this breaks due to a Docker/Firefly III upgrade. **Caveat emptor!**

## How do I use this?

I’ve only tested this on DigitalOcean Ubuntu 18.04LTS, but it *should* work similarly for other providers:

0. Make sure your DNS is correctly set up for your provider, or Let’s Encrypt will fail.

1. Install `docker` + `docker-compose`. I used the [DigitalOcean one-click droplet](https://do.co/2PQDXut). Here’s the versions I tested this with:

```
$ docker --version
Docker version 18.09.2, build 6247962
$ docker-compose --version
docker-compose version 1.22.0, build f46880fe
```

2. Drop the `docker-compose.yml` into your favorite directory and make the necessary edits to the `XXXX` fields. Make sure that you read the[Firefly III docs on Docker Compose](https://firefly-iii.readthedocs.io/en/latest/installation/docker.html#docker-hub-with-automatic-updates-via-docker-compose) as it contains important information on the choice of keys/passwords!

3. Run `docker network create --driver bridge reverse-proxy` to create the reverse proxy network bridge.

4. Run `docker-compose up` and wait for the service to come up.

If everything was set up correctly, navigating to https:://example.com should yield the Firefly III registration page.

## Troubleshooting

### HTTPS Isn’t Working!

If you’re having trouble setting up the certs, try first setting up using the provided `docker-compose.apache.yml`.

It should correctly set up the certs, and you can re-use the `nginx` containers and replace the Apache one with the Firefly III ones.

[Always make sure your DNS is set up properly](https://www.digitalocean.com/community/tutorials/how-to-point-to-digitalocean-nameservers-from-common-domain-registrars).

### I did a poopy, how do I reset?

Try doing `docker-compose down && docker-compose rm`. It usually fixes most issues.

You can always try nuking everything from orbit using [`docker-nuke`](https://github.com/levi-rs/docker-nuke) and starting over. **YOU WILL LOSE DATA.**

### nginx gives me a 502 error!

Make sure that you created the separate bridge network (`docker-compose` _should_ fail if you skip that step).

Then, ensure that the Firefly III app (and _only_ the app) is on both the `reverse-proxy` and `firefly_iii_net` networks.

## References/Useful External Links

+ [Firefly III docs on Docker Compose](https://firefly-iii.readthedocs.io/en/latest/installation/docker.html#docker-hub-with-automatic-updates-via-docker-compose)
+ [Google’s guide on an `nginx` reverse-proxy with Let’s Encrypt HTTPS](https://cloud.google.com/community/tutorials/nginx-reverse-proxy-docker)
+ [Firefly III issue on Firefly in docker behind reverse proxy](https://cloud.google.com/community/tutorials/nginx-reverse-proxy-docker)
+ [Using depends and volumes for Docker 3+ for `letsencrypt-nginx-proxy-companion`](https://cloud.google.com/community/tutorials/nginx-reverse-proxy-docker)
+ [DigitalOcean one-click Docker droplet](https://do.co/2PQDXut)
+ [DigitalOcean DNS setup guide](https://www.digitalocean.com/community/tutorials/how-to-point-to-digitalocean-nameservers-from-common-domain-registrars)
+ [Let’s Encrypt nginx proxy companion Docker Github page](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion)
+ [`nginx` proxy Docker container Github page](https://github.com/jwilder/nginx-proxy)
+ [SendGrid, which I use for the automatic emails](https://sendgrid.com/)

## Disclaimer

Read the LICENSE.md file for important disclaimer and copyright information.
